<!DOCTYPE html>
<html>

<head>
	<title>Fernando Salom</title>
	<meta property="og:title" content="¿Quién soy?" />
	<meta property="og:image" content="http://www.fernandosalom.es/images/fernandofacebook.jpg"/>
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:600,600italic,700italic,700,400,400italic,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/general.css" />	
    <link rel="stylesheet" type="text/css" href="css/fondo.css" />	
           
    <script src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript">
    
    $("document").ready(function(){	    	
	    $(".OpenList").click(function(){			
			$(".listToDo").toggle();
		});
    });
    </script>

</head>
<body>

		<ul class="fondo">
            <li><span>Image 01</span><div><h3>Seul (Corea del Sur)</h3></div></li>
            <li><span>Image 02</span><div><h3>Santa Monica (EEUU)</h3></div></li>            
            <li><span>Image 03</span><div><h3>San Franciso (EEUU)</h3></div></li>
            <li><span>Image 04</span><div><h3>Tokyo (Japón)</h3></div></li>
            <li><span>Image 05</span><div><h3>Beijing (China)</h3></div></li>
            <li><span>Image 06</span><div><h3>Shanghai (China)</h3></div></li>
            <li><span>Image 07</span><div><h3>Hong Kong (China)</h3></div></li>
            <li><span>Image 08</span><div><h3>Nueva York (EEUU)</h3></div></li>
            <li><span>Image 09</span><div><h3>Miami (EEUU)</h3></div></li>
            <li><span>Image 10</span><div><h3>Las Vegas (EEUU)</h3></div></li>
        </ul>
        <div class="contenedor">           
            <header>
                <div id="envoltorio">
					
					<div id="contenido">
						<div id="cabecera">
							<center><img src="images/fernandosalom.png"></center>
							<div class="line"></div>
							<h2>
								Vivo en un continuo cambio
							</h2>
							<div class="line"></div>
						</div>
						<div id="cuerpo">
							<p>
								¿Quien soy?... buena pregunta, ¿sabrías definirte si alguien te hace esa pregunta?. Creo que para describir quien soy tendría que decir antes quien he sido, nuestro pasado nos define y supongo que al igual que para muchos otros una decisión es el punto de inflexión que lo cambia todo.								
							</p>
							<p>								
								 Había estudiado y vivido toda mi vida cerca de mi zona de confort y de todo aquello que conocía. Estudié Ingeniería Informática porque siempre me gustó trastear con ordenadores y cuando estaba en el último curso tuve una pequeña "revelación". Me vi en tercera persona y me hice 3 preguntas:
								¿Quién eres?, ¿Eres cómo te imaginabas hace años? y ¿Quién quieres ser?. Mis respuestas me dejaron claro que necesitaba un cambio. Así que me puse a buscar una opción que me acercase a aquella persona que quería ser y la encontré.
							</p>
							<p>
								Decidí estudiar un master en Administración de Empresas (MBA) impartido en ingles lo más lejos de todo lo que conocía que pude. Dicen que la distancia te hace ver las cosas de forma más objetiva y justo eso era lo que yo necesitaba. Lo que empezó siendo un pequeño paso para encontrarme acabo siendo
								la mejor experiencia que he tenido en mi vida (al menos hasta el momento). He vivido en 3 ciudades diferentes alrededor del mundo durante 1 año (Madrid, Shanghai e Irvine-California). He dado la vuelta al mundo sin pasar por casa, descubriendo cientos de sitios nuevos, conociendo a personas increíbles a las que puedo llamar amigos
								y por supuesto disfrutando como jamás me hubiera imaginado. Dicen que la vida es un conjunto de decisiones, estás donde estás por las decisiones que tomaste en el pasado, y no puedo estar más de acuerdo.
							</p>
							<p>
								Volví a casa, pero ya no era el mismo. Cree mi primera empresa [Superdry Valencia] con mi mejor amigo a quien conocí en el master y re-descubrí a mi familia y amigos. El mundo es más pequeño ahora, las inquietudes son otras y veo la vida con otros ojos. No puedo decirte quien soy porque mañana no seré el mismo, pero si quien quiero ser.
							</p>
							<p style="text-align:right;">
								Vive y disfruta,<br/>
								Fernando.
							</p>
							<!-- 
							<p>
								Desde pequeño me ha gustado trastear con ordenadores. Comence aprendiendo muchas de los lenguajes de programación que se actualmente de forma autodidacta, hasta entrar en la universidad para estudiar Ingeniería Informática. Aunque en el momento de tomar la decisión de entrar en la universidad 
								acabe inclinandome por estudiar Derecho (tradición familiar). A diferencia de lo que puedes pensar no fue un error, gracias a 
								ello madure, me di cuenta de las cosas que quería en mi vida y siempre digo que si volviera atrás lo volvería a hacer. Al año siguiente empece Ingeniería Informática
								y ha sido otra de las decisiones de las que no me arrepiento a pesar del mercado laboral español.  
							</p>
							<p>
								Al acabar la carrera y con el objetivo de saber más sobre el mundo de la empresa, que tanto me había llamado la atención desde pequeño, hice un MBA. Con la particularidad
								de que este master se impartia en 3 sedes alrededor del mundo Madrid(España), Shanghai(China) y Irvine (EEUU). Ha sido la experiencia más gratificante y de la que más aprendido
								en mi vida, 1 año fuera de casa dando la vuelta por el mundo y conociendo a gente muy importante para mi a día de hoy. 
							</p>	
							<p>
								En 2012 tras volver a España, y con mi titulo de MBA bajo el brazo me disponía a enfrentarme a la crisis cara a cara. Cree mi primera empresa [Gomu Gomu 2011 S.L.] con mi amigo y socio Richard a quien conocí en el master.
								Montamos la primera Superdry Store de Valencia. Una marca de ropa casual novedad en el mercado español, pero muy bien posicionada fuera de nuestras fronteras. Y aun seguimos!
								
							</p>	
							<p>
								Actualmente ejerzo como Ingeniero de Software para una consultora con el objetivo de seguir aprendiendo, pero siempre con la vista puesta en continuar emprendiendo y poder vivir
								algún día de mis ideas.								
							</p>		 
							<p class="tright">
								Aun queda mucho por escribir...
							</p>	
							 -->	
						</div> 						
						<div id="pie">
							<!-- 
							<div class="linebreak"></div>
								<div class="OpenList">Mi To-Do list</div>
								<div class="listToDo">									
										<p>
											<input type="checkbox" checked disabled> <strike>Estudiar una carrera</strike><br/>
											<input type="checkbox" checked disabled> <strike>Dar la vuelta al mundo</strike><br/>
											<input type="checkbox" checked disabled> <strike>Vivir en el extranjero</strike><br/>							
											<input type="checkbox" checked disabled> <strike>Saltar en paracaidas</strike><br/>
											<input type="checkbox" checked disabled> <strike>Hacer flyboard</strike><br/>
											<input type="checkbox"  disabled> Completar una maratón <br/>
											<input type="checkbox"  disabled> Descenso en aguas bravas <br/>
											<input type="checkbox"  disabled> Bautismo de buceo<br/>							
											<input type="checkbox"  disabled> Puenting<br/>
											<input type="checkbox"  disabled> Coger un vuelo al azar <br/>							
											<input type="checkbox"  disabled> Hacer un videojuego <br/>
										</p>									
										
								</div>
								--> 
							<div class="linebreak"></div>
									<div class="openCV"><a href="http://www.linkedin.com/in/fsalom">¿Quieres ver mi CV?</a></div>
								<!--<div class="underCV">aqui esta</div>-->
							<div class="linebreak"></div>
							<!-- 	
								<div class="openCV"><a href="#">¿Quieres contactar conmigo?</a></div>
							<div class="linebreak"></div>
							 -->
						</div>
						
					</div>
				</div>
            </header>
        </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1359233-2', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>